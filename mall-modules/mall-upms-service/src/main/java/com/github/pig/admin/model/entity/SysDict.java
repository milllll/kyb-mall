/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.github.pig.admin.model.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author lengleng
 * @since 2017-11-19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Builder
@TableName("sys_dict" )
public class SysDict extends Model<SysDict> {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.UUID)
    @TableField("id")
    private String id;
    /**
     * 数据值
     */
    @TableField("value" )
    private String value;
    /**
     * 标签名
     */
    @TableField("label" )
    private String label;
    /**
     * 类型
     */
    @TableField("type" )
    private String type;
    /**
     * 描述
     */
    @TableField("description" )
    private String description;

    /**
     * 描述
     */
    @TableField("parent_label" )
    private String parentLabel;
    /**
     * 排序（升序）
     */
    @TableField("sort" )
    private BigDecimal sort;
    /**
     * 创建时间
     */
    @TableField("create_time" )
    private Date createTime;
    /**
     * 更新时间
     */
    @TableField("update_time" )
    private Date updateTime;

    /**
     * 更新人
     */
    @TableField("creator" )
    private String creator;


    /**
     * 修改人
     */
    @TableField("editor" )
    private String editor;


    /**
     * 更新时间
     */
    @TableField("level" )
    private Integer level;
    /**
     * 备注信息
     */
    @TableField("remarks" )
    private String remarks;
    /**
     * 删除标记
     */
    @TableField("del_flag" )
    private String delFlag;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}

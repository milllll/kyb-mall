package com.github.pig.goods.bussiness.product.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 * 天窗（轮播图） false
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
@Data
@EqualsAndHashCode()
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Builder
@TableName("t_sky_light")
public class TSkyLight extends Model<TSkyLight> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */

    @TableId(value = "id", type = IdType.UUID)
    @TableField("id")
    private String id;
    /**
     * 天窗ID
     */

    @TableField("sky_id")
    private String skyId;
    /**
     * 天窗类型：Banner
     */

    @TableField("sky_type")
    private String skyType;
    /**
     * 天窗内容
     */

    @TableField("sky_content")
    private String skyContent;
    /**
     * 天窗标题
     */

    @TableField("sky_title")
    private String skyTitle;
    /**
     * 描述
     */

    @TableField("description")
    private String description;
    /**
     * 状态 0:解锁 1：锁定
     */

    @TableField("status")
    private Integer status;


    /**
     * 创建者
     */

    @TableField("creator")
    private String creator;
    /**
     * 创建时间
     */

    @TableField("create_time")
    private Date createTime;
    /**
     * 更新时间
     */

    @TableField("update_time")
    private Date updateTime;
    /**
     * 更新者
     */

    @TableField("editer")
    private String editer;


    @TableField("del_flag")
    private String delFlag;


    @TableField("position")
    private Integer position;



    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}

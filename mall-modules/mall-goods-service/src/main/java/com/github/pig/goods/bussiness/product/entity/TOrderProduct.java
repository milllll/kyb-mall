package com.github.pig.goods.bussiness.product.entity;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单产品关联表 false
 * </p>
 *
 * @author tao123
 * @since 2018-09-13
 */
@Data
@EqualsAndHashCode()
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("t_order_product")
public class TOrderProduct extends Model<TOrderProduct> {

    private static final long serialVersionUID = 1L;


    @TableId(value = "id", type = IdType.UUID)
    @TableField("id")
    private String id;
    /**
     * 订单ID
     */

    @TableField("order_id")
    private String orderId;
    /**
     * 产品ID
     */

    @TableField("product_id")
    private String productId;
    /**
     * 数量
     */

    @TableField("quantity")
    private Integer quantity;
    /**
     * 单位
     */

    @TableField("unit")
    private String unit;
    /**
     * 价格
     */

    @TableField("price")
    private Integer price;
    /**
     * 创建时间
     */

    @TableField("create_time")
    private Date createTime;


    /**
     * 更新时间
     */

    @TableField("update_time")
    private Date updateTime;



    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}

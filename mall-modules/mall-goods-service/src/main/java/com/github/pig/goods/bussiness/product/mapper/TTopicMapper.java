package com.github.pig.goods.bussiness.product.mapper;

import com.github.pig.goods.bussiness.product.entity.TTopic;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 主题表 Mapper 接口
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
public interface TTopicMapper extends BaseMapper<TTopic> {

}

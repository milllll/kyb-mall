package com.github.pig.goods.bussiness.product.controller;
import com.github.pig.common.util.UserUtils;
import com.github.pig.goods.bussiness.product.productEnum.SkyLightEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;


import com.github.pig.common.constant.CommonConstant;
import com.github.pig.common.util.Query;
import com.github.pig.common.util.R;
import com.github.pig.common.web.BaseController;
import com.github.pig.goods.bussiness.product.service.ITSkyLightService;
import com.github.pig.goods.bussiness.product.entity.TSkyLight;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;

import java.util.Date;
import java.util.Map;

/**
 * <p>
 * 天窗（轮播图） 前端控制器
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
@RestController
@RequestMapping("/tSkyLight")
@Api(value = "TSkyLightController", description = "天窗表")
public class TSkyLightController extends BaseController{


@Autowired
private ITSkyLightService tSkyLightService;

/**
 * 通过ID查询
 *
 * @param id ID
 * @return TSkyLight
 */
@GetMapping("/{id}")
@ApiOperation(value = "  按id查询", httpMethod = "GET", notes = "  按id查询")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<TSkyLight> get(@PathVariable String id){
    return new R<>(tSkyLightService.selectById(id));
}


/**
* 分页查询信息
*
* @param params 分页对象
* @return 分页对象
*/
@RequestMapping("/page")
@ApiOperation(value = "分页查询", httpMethod = "GET",notes = "TSkyLight列表方法")
@ApiImplicitParam(name = "查询参数", value = "TSkyLight参数", required = true, dataType = "Map", paramType = "path")
public Page page(@RequestParam Map<String, Object> params){
    params.put(CommonConstant.DEL_FLAG,CommonConstant.STATUS_NORMAL);
    params.put(SkyLightEnum.UNLOCK.getColumn(),SkyLightEnum.UNLOCK.getIndex());
    return tSkyLightService.selectPage(new Query<>(params),new EntityWrapper<>());
}

/**
 * 添加
 * @param  tSkyLight 实体
 * @return success/false
 */
@PostMapping("/add")
@ApiOperation(value = "新增实体", notes = "TSkyLight新增方法")
@ApiImplicitParam(name = "tSkyLight", value = "TSkyLight实体", required = true, dataType = "TSkyLight", paramType = "path")
public R<Boolean> add(@RequestBody TSkyLight tSkyLight){
    tSkyLight.setDelFlag(CommonConstant.STATUS_NORMAL).setStatus(SkyLightEnum.UNLOCK.getIndex()).setCreateTime(new Date()).setCreator(UserUtils.getUser());
    return new R<>(tSkyLightService.insert(tSkyLight));
}

/**
 * 删除
 * @param id ID
 * @return success/false
 */
@DeleteMapping("/{id}")
@ApiOperation(value = "删除", notes = "TSkyLight删除")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<Boolean> delete(@PathVariable String id){return new R<>(tSkyLightService.deleteById(id));}

/**
 * 编辑
 * @param  tSkyLight 实体
 * @return success/false
 */
@PutMapping("/update")
@ApiOperation(value = "修改", notes = "TSkyLight修改")
@ApiImplicitParam(name = "tSkyLight", value = "TSkyLight实体", required = true, dataType = "TSkyLight", paramType = "path")
public R<Boolean> edit(@RequestBody TSkyLight tSkyLight){
    tSkyLight.setUpdateTime(new Date());
    return new R<>(tSkyLightService.updateById(tSkyLight));
}


}

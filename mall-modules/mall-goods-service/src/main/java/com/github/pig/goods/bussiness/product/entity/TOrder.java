package com.github.pig.goods.bussiness.product.entity;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单主表 false
 * </p>
 *
 * @author tao123
 * @since 2018-09-13
 */
@Data
@EqualsAndHashCode()
@Accessors(chain = true)
@Builder
@TableName("t_order")
@AllArgsConstructor
@NoArgsConstructor
public class TOrder extends Model<TOrder> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */

    @TableId(value = "id", type = IdType.UUID)
    @TableField("id")
    private String id;
    /**
     * 用户ID
     */

    @TableField("account_id")
    private String accountId;

    /**
     * 用户openId
     */

    @TableField("open_id")
    private String openId;

    /**
     * 订单类型：Normal 正常
     */

    @TableField("type")
    private String type;
    /**
     * 订单状态：WaitForPay 0-待支付，WaitForDeliver 1-待发货，Shipped 2-已发货，Finished 3-已完成，Cancled 4-已取消
     */

    @TableField("status")
    private String status;
    /**
     * 收货地址、 json 格式
     */

    @TableField("delivery_address")
    private String deliveryAddress;
    /**
     * 快递公司
     */

    @TableField("express_company_name")
    private String expressCompanyName;
    /**
     * 快递单号
     */

    @TableField("express_tracking_no")
    private String expressTrackingNo;
    /**
     * 支付方式：0-在线支付, 1-货到付款，2-公司转账
     */

    @TableField("payment_method")
    private String paymentMethod;
    /**
     * 支付渠道：0-微信Wechat, 1-支付宝,Alipay,2-银联
     */

    @TableField("payment_channel")
    private String paymentChannel;

    /**
     * 订单支付单号：第三方支付流水号
     */

    @TableField("payment_no")
    private String paymentNo;


    /**
     * 预支付订单支付单号：第三方支付流水号
     */

    @TableField("prepay_id")
    private String prepayId;
    /**
     * 支付状态：0-支付成功,1-支付失败
     */

    @TableField("payment_status")
    private String paymentStatus;
    /**
     * 订单应付金额
     */

    @TableField("order_amount_payable")
    private Integer orderAmountPayable;
    /**
     * 订单实付金额
     */

    @TableField("order_amount_payment")
    private Integer orderAmountPayment;
    /**
     * 返点金额
     */

    @TableField("rebate_total")
    private Double rebateTotal;
    /**
     * 是否返点，0-是，1-否
     */

    @TableField("rebate_status")
    private String rebateStatus;
    /**
     * 代理商ID
     */

    @TableField("agent_id")
    private String agentId;
    /**
     * 买家留言
     */

    @TableField("note")
    private String note;
    /**
     * 支付时间
     */

    @TableField("pay_time")
    private Date payTime;
    /**
     * 配送方式：0-快递配送, 1-上门自提
     */

    @TableField("delivery_method")
    private String deliveryMethod;
    /**
     * 发货时间
     */

    @TableField("shipping_time")
    private Date shippingTime;
    /**
     * 收货时间
     */

    @TableField("received_time")
    private Date receivedTime;
    /**
     * 创建时间
     */

    @TableField("created_time")
    private Date createdTime;
    /**
     * 更新时间
     */

    @TableField("update_time")
    private Date updateTime;



    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}

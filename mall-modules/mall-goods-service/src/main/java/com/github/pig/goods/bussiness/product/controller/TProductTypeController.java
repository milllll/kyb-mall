package com.github.pig.goods.bussiness.product.controller;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.feilong.core.DatePattern;
import com.feilong.core.date.DateUtil;
import com.github.pig.common.constant.CommonConstant;
import com.github.pig.common.util.ConvertMapUtil;
import com.github.pig.common.util.Query;
import com.github.pig.common.util.R;
import com.github.pig.common.util.UserUtils;
import com.github.pig.common.web.BaseController;
import com.github.pig.goods.bussiness.product.entity.TProductType;
import com.github.pig.goods.bussiness.product.service.ITProductTypeService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 字典表 前端控制器
 * </p>
 *
 * @author Carlos123
 * @since 2018-11-22
 */
@RestController
@Slf4j
@RequestMapping("/tProductType")
public class TProductTypeController extends BaseController {


    @Autowired
    private ITProductTypeService tProductTypeService;

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return TProductType
     */
    @GetMapping("/{id}")
    @ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
    public R<TProductType> get(@PathVariable String id) {
        return new R<>(tProductTypeService.selectById(id));
    }


    /**
     * 分页查询信息
     *
     * @param params 分页对象
     * @return 分页对象
     */
    @RequestMapping("/page")
    @ApiOperation(value = "分页查询", notes = "TProductType列表方法")
    @ApiImplicitParam(name = "查询参数", value = "TProductType参数", required = true, dataType = "Map", paramType = "path")
    public Page page(@RequestParam Map<String, Object> params) {
        params.put(CommonConstant.DEL_FLAG, CommonConstant.STATUS_NORMAL);
        if (params.get("startTime") != null && params.get("endTime") != null) {
            Date startTime = DateUtil.toDate(params.get("startTime").toString(), DatePattern.COMMON_DATE);
            Date endTime = DateUtil.toDate(params.get("endTime").toString(), DatePattern.COMMON_DATE);
            params.remove("startTime");
            params.remove("endTime");
            return tProductTypeService.selectPage(new Query<>(ConvertMapUtil.convertMapOfUnderLine(params)),
                    Condition.create().between("create_time", startTime, endTime).orderBy("sort",true));
        }
        return tProductTypeService.selectPage(new Query<>(ConvertMapUtil.convertMapOfUnderLine(params)), new EntityWrapper<>(new TProductType()).orderBy("sort",true));
    }

    /**
     * 查询产品类别信息
     *
     * @param params 分页对象
     * @return 分页对象
     */
    @RequestMapping("/dictList")
    @ApiOperation(value = "查询 参数为SysDict表", httpMethod = "GET", notes = "")
    public List<TProductType> dictList(@RequestParam Map<String, Object> params) {
        params.put(CommonConstant.DEL_FLAG, CommonConstant.STATUS_NORMAL);
        if (params.get("startTime") != null && params.get("endTime") != null && params.get("label") != null) {
            Date startTime = DateUtil.toDate(params.get("startTime").toString(), DatePattern.COMMON_DATE);
            Date endTime = DateUtil.toDate(params.get("endTime").toString(), DatePattern.COMMON_DATE);
            return tProductTypeService.selectList(Condition.create().eq("label", params.get("label")).between("create_time", startTime, endTime).orderBy("sort",true));
        }
        Page<TProductType> page= tProductTypeService.selectPage(new Query<>(ConvertMapUtil.convertMapOfUnderLine(params)),
                new EntityWrapper<>(new TProductType()).orderBy("sort",true));
        return page.getRecords();
    }


    /**
     * 通过字典类型查找字典
     *
     * @param label 类型
     * @return 同类型字典
     */
    @GetMapping("/label/{label}")
    @ApiOperation(value = "通过字典类型查找字典参数为label", httpMethod = "GET", notes = "")
    @Cacheable(value = "dict_details", key = "#label")
    public List<TProductType> findDictByLabel(@PathVariable String label) {
        TProductType condition = new TProductType();
        condition.setDelFlag(CommonConstant.STATUS_NORMAL);
        condition.setType(label);
        return tProductTypeService.selectList(new EntityWrapper<>(condition));
    }


    /**
     * 添加
     *
     * @param tProductType 实体
     * @return success/false
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增实体", notes = "TProductType新增方法")
    @ApiImplicitParam(name = "tProductType", value = "TProductType实体", required = true, dataType = "TProductType", paramType = "path")
    public R<Boolean> add(@RequestBody TProductType tProductType) {
        tProductType.setCreator(UserUtils.getUser());
        tProductType.setCreateTime(new Date());
        tProductType.setDelFlag(CommonConstant.STATUS_NORMAL);
        System.out.println(tProductType.toString());
        return new R<>(tProductTypeService.insert(tProductType));
    }

    /**
     * 删除
     *
     * @param id ID
     * @return success/false
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除", notes = "TProductType删除")
    @ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
    public R<Boolean> delete(@PathVariable String id) {
        return new R<>(tProductTypeService.deleteById(id));
    }

    /**
     * 编辑
     *
     * @param tProductType 实体
     * @return success/false
     */
    @PutMapping("/update")
    @ApiOperation(value = "修改", notes = "TProductType修改")
    @ApiImplicitParam(name = "tProductType", value = "TProductType实体", required = true, dataType = "TProductType", paramType = "path")
    public R<Boolean> edit(@RequestBody TProductType tProductType) {
        tProductType.setUpdateTime(new Date());
        return new R<>(tProductTypeService.updateById(tProductType));
    }


}

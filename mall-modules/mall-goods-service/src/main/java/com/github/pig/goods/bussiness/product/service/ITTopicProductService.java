package com.github.pig.goods.bussiness.product.service;

import com.github.pig.goods.bussiness.product.entity.TTopicProduct;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 主题产品关联表 服务类
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
public interface ITTopicProductService extends IService<TTopicProduct> {

}

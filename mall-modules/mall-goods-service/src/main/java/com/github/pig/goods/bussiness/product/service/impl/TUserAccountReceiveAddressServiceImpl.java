package com.github.pig.goods.bussiness.product.service.impl;

import com.github.pig.goods.bussiness.product.entity.TUserAccountReceiveAddress;
import com.github.pig.goods.bussiness.product.mapper.TUserAccountReceiveAddressMapper;
import com.github.pig.goods.bussiness.product.service.ITUserAccountReceiveAddressService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户收货地址表 服务实现类
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
@Service
public class TUserAccountReceiveAddressServiceImpl extends ServiceImpl<TUserAccountReceiveAddressMapper, TUserAccountReceiveAddress> implements ITUserAccountReceiveAddressService {

}

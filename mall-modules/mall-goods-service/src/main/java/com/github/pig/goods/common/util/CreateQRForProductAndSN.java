package com.github.pig.goods.common.util;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

@Slf4j
public class CreateQRForProductAndSN {

    public static URL getQRCode(HttpServletRequest request,String sameStyleNum,String productId,String sn)throws Exception{
        String targetUrl = "http://m.fabricdealing.com/productDetail/"+productId+"?sn="+sn;
        String path = request.getServletContext().getRealPath("/")+"result.png";
        log.info("当前二维码地址为：{}",path);
        BufferedImage image = QRCodeKit.createQRCode(targetUrl);
        QRCodeKit.draw(image,path,path,sameStyleNum,sn);
        File newFile = new File(path);
        URL QRUrl= UploadFile.picCOS(newFile,"QRCode",sn+sameStyleNum);
        newFile.delete();
        return  QRUrl;
    }

}

package com.github.pig.goods.bussiness.product.entity;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户表 false
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode()
@Accessors(chain = true)
@Builder
@TableName("t_user_account")
public class TUserAccount extends Model<TUserAccount> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */

    @TableId(value = "id", type = IdType.UUID)
    @TableField("id")
    private String id;
    /**
     * 密码，MD5加密
     */

    @TableField("password")
    private String password;
    /**
     * 手机号码，登录时用手机号码+密码登录
     */

    @TableField("phone_number")
    private String phoneNumber;
    /**
     * 昵称
     */

    @TableField("name")
    private String name;
    /**
     * 邮箱
     */

    @TableField("email")
    private String email;
    /**
     * 头像
     */

    @TableField("url")
    private String url;
    /**
     * 用户来源：0-公众号,1-小程序,2-App,3-网站
     */

    @TableField("regist_channel")
    private String registChannel;
    /**
     * 创建时间
     */

    @TableField("create_time")
    private Date createTime;
    /**
     * 更新时间
     */

    @TableField("update_time")
    private Date updateTime;
    /**
     * 最近登录时间
     */

    @TableField("latest_login_time")
    private Date latestLoginTime;
    /**
     * 状态：0正常 1封号
     */

    @TableField("status")
    private String status;

    /**
     * openId
     */

    @TableField("open_id")
    private String openId;


    @TableField("del_flag")
    private Integer delFlag = 0;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}

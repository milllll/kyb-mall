package com.github.pig.goods.bussiness.product.service.impl;

import com.github.pig.goods.bussiness.product.entity.TTopicProduct;
import com.github.pig.goods.bussiness.product.mapper.TTopicProductMapper;
import com.github.pig.goods.bussiness.product.service.ITTopicProductService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 主题产品关联表 服务实现类
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
@Service
public class TTopicProductServiceImpl extends ServiceImpl<TTopicProductMapper, TTopicProduct> implements ITTopicProductService {

}

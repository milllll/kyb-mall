package com.github.pig.goods.bussiness.product.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pig.common.util.Query;
import com.github.pig.goods.bussiness.product.entity.TOrder;
import com.github.pig.goods.bussiness.product.entity.VoOrder;
import com.github.pig.goods.bussiness.product.mapper.TOrderMapper;
import com.github.pig.goods.bussiness.product.service.ITOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单主表 服务实现类
 * </p>
 *
 * @author tao123
 * @since 2018-09-13
 */
@Service(value = "TOrderServiceImpl")
public class TOrderServiceImpl extends ServiceImpl<TOrderMapper, TOrder> implements ITOrderService {


    @Autowired
    private TOrderMapper tOrderMapper;

    @Override
    public Page selectWithOrderPage(Query query) {
        query.setRecords(tOrderMapper.selectOrderPage(query,query.getCondition()));
        return query;
    }

    @Override
    public Page selectOrderForMobile(Query query) {
        query.setRecords(tOrderMapper.selectOrderForMobile(query,query.getCondition()));
        return query;
    }

    @Override
    public VoOrder selectOrderById(String id) {
        return tOrderMapper.selectOrderById(id);
    }
}

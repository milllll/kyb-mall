package com.github.pig.goods.bussiness.product.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 主题表 false
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
@Data
@EqualsAndHashCode()
@Accessors(chain = true)
@Builder
@TableName("t_topic")
public class TTopic extends Model<TTopic> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */

    @TableId(value = "id", type = IdType.UUID)
    @TableField("id")
    private String id;
    /**
     * 标题
     */

    @TableField("title")
    private String title;
    /**
     * 描述
     */

    @TableField("description")
    private String description;
    /**
     * 顺序
     */

    @TableField("sort")
    private Long sort;
    /**
     * 开始时间
     */

    @TableField("start_time")
    private Date startTime;
    /**
     * 结束时间
     */

    @TableField("end_time")
    private Date endTime;
    /**
     * 状态
     */

    @TableField("status")
    private String status;
    /**
     * 创建时间
     */

    @TableField("create_time")
    private Date createTime;
    /**
     * 更新时间
     */

    @TableField("update_time")
    private Date updateTime;
    /**
     * 创建者
     */

    @TableField("creator")
    private String creator;
    /**
     * 更新者
     */

    @TableField("editer")
    private String editer;

    public TTopic(String id, String title, String description, Long sort, Date startTime, Date endTime, String status, Date createTime, Date updateTime, String creator, String editer) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.sort = sort;
        this.startTime = startTime;
        this.endTime = endTime;
        this.status = status;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.creator = creator;
        this.editer = editer;
    }

    public TTopic() { }
    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}

package com.github.pig.goods.bussiness.product.service.impl;

import com.github.pig.goods.bussiness.product.entity.TSkyLight;
import com.github.pig.goods.bussiness.product.mapper.TSkyLightMapper;
import com.github.pig.goods.bussiness.product.service.ITSkyLightService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 天窗（轮播图） 服务实现类
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
@Service
public class TSkyLightServiceImpl extends ServiceImpl<TSkyLightMapper, TSkyLight> implements ITSkyLightService {

}

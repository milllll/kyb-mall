package com.github.pig.goods.bussiness.product.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pig.common.util.Query;
import com.github.pig.goods.bussiness.product.entity.TProduct;
import com.github.pig.goods.bussiness.product.entity.ToProduct;
import com.github.pig.goods.bussiness.product.mapper.TProductMapper;
import com.github.pig.goods.bussiness.product.service.ITProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 产品表 服务实现类
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-23
 */
@Slf4j
@Service
public class TProductServiceImpl extends ServiceImpl<TProductMapper, TProduct> implements ITProductService {

    @Autowired
    private TProductMapper tProductMapper;

    @Override
    public Page selectWithProductPage(Query query) {
        log.info("------"+query.getCondition());
        query.setRecords(tProductMapper.selectProductPage(query,query.getCondition()));
        return query;
    }

    @Override
    public ToProduct getProductById(String id) {
        return tProductMapper.getProductById(id);
    }
}

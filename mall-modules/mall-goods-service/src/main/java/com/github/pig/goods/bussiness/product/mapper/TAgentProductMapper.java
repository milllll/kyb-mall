package com.github.pig.goods.bussiness.product.mapper;

import com.github.pig.goods.bussiness.product.entity.TAgentProduct;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 代理商产品关联表 Mapper 接口
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
public interface TAgentProductMapper extends BaseMapper<TAgentProduct> {

}

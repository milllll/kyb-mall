package com.github.pig.goods.bussiness.product.mapper;

import com.github.pig.goods.bussiness.product.entity.TProductType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author Carlos123
 * @since 2018-11-22
 */
public interface TProductTypeMapper extends BaseMapper<TProductType> {

}

package com.github.pig.goods.bussiness.product.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;


import com.github.pig.common.constant.CommonConstant;
import com.github.pig.common.util.Query;
import com.github.pig.common.util.R;
import com.github.pig.common.web.BaseController;
import com.github.pig.goods.bussiness.product.service.ITTopicProductService;
import com.github.pig.goods.bussiness.product.entity.TTopicProduct;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;

import java.util.Date;
import java.util.Map;

/**
 * <p>
 * 主题产品关联表 前端控制器
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
@RestController
@RequestMapping("/tTopicProduct")
@Api(value = "TTopicProductController", description = "主题产品关联表")
public class TTopicProductController extends BaseController{


@Autowired
private ITTopicProductService tTopicProductService;

/**
 * 通过ID查询
 *
 * @param id ID
 * @return TTopicProduct
 */
@GetMapping("/{id}")
@ApiOperation(value = "  按id查询", httpMethod = "GET", notes = "  按id查询")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<TTopicProduct> get(@PathVariable String id){
    return new R<>(tTopicProductService.selectById(id));
}


/**
* 分页查询信息
*
* @param params 分页对象
* @return 分页对象
*/
@RequestMapping("/page")
@ApiOperation(value = "分页查询",httpMethod = "GET", notes = "TTopicProduct列表方法")
@ApiImplicitParam(name = "查询参数", value = "TTopicProduct参数", required = true, dataType = "Map", paramType = "path")
public Page page(@RequestParam Map<String, Object> params){
    params.put(CommonConstant.DEL_FLAG,CommonConstant.STATUS_NORMAL);
    return tTopicProductService.selectPage(new Query<>(params),new EntityWrapper<>());
}

/**
 * 添加
 * @param  tTopicProduct 实体
 * @return success/false
 */
@PostMapping("/add")
@ApiOperation(value = "新增实体", notes = "TTopicProduct新增方法")
@ApiImplicitParam(name = "tTopicProduct", value = "TTopicProduct实体", required = true, dataType = "TTopicProduct", paramType = "path")
public R<Boolean> add( TTopicProduct tTopicProduct){
    return new R<>(tTopicProductService.insert(tTopicProduct));
}

/**
 * 删除
 * @param id ID
 * @return success/false
 */
@DeleteMapping("/{id}")
@ApiOperation(value = "删除", notes = "TTopicProduct删除")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<Boolean> delete(@PathVariable String id){return new R<>(tTopicProductService.deleteById(id));}

/**
 * 编辑
 * @param  tTopicProduct 实体
 * @return success/false
 */
@PutMapping("/update")
@ApiOperation(value = "修改", notes = "TTopicProduct修改")
@ApiImplicitParam(name = "tTopicProduct", value = "TTopicProduct实体", required = true, dataType = "TTopicProduct", paramType = "path")
public R<Boolean> edit(@RequestBody TTopicProduct tTopicProduct){
    tTopicProduct.setUpdateTime(new Date());
    return new R<>(tTopicProductService.updateById(tTopicProduct));
}


}

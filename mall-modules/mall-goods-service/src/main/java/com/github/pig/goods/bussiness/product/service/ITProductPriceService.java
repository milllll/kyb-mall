package com.github.pig.goods.bussiness.product.service;

import com.github.pig.goods.bussiness.product.entity.TProductPrice;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 产品价格表 服务类
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
public interface ITProductPriceService extends IService<TProductPrice> {

}

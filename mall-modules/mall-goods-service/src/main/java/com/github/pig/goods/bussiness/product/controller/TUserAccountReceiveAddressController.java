package com.github.pig.goods.bussiness.product.controller;
import com.baomidou.mybatisplus.mapper.Condition;
import com.github.pig.common.util.ConvertMapUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;


import com.github.pig.common.constant.CommonConstant;
import com.github.pig.common.util.Query;
import com.github.pig.common.util.R;
import com.github.pig.common.web.BaseController;
import com.github.pig.goods.bussiness.product.service.ITUserAccountReceiveAddressService;
import com.github.pig.goods.bussiness.product.entity.TUserAccountReceiveAddress;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户收货地址表 前端控制器
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
@RestController
@RequestMapping("/tUserAccountReceiveAddress")
@Api(value = "TUserAccountReceiveAddressController", description = "用户收货地址表")
public class TUserAccountReceiveAddressController extends BaseController{


@Autowired
private ITUserAccountReceiveAddressService tUserAccountReceiveAddressService;

/**
 * 通过ID查询
 *
 * @param id ID
 * @return TUserAccountReceiveAddress
 */
@GetMapping("/{id}")
@ApiOperation(value = "  按id查询", httpMethod = "GET", notes = "  按id查询")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<TUserAccountReceiveAddress> get(@PathVariable String id){
    return new R<>(tUserAccountReceiveAddressService.selectById(id));
}


/**
* 分页查询信息
*
* @param params 分页对象
* @return 分页对象
*/
@RequestMapping("/page")
@ApiOperation(value = "分页查询 参数为tUserAccountReceiveAddress表中的属性",httpMethod = "GET", notes = "TUserAccountReceiveAddress列表方法")
@ApiImplicitParam(name = "查询参数", value = "TUserAccountReceiveAddress参数", required = true, dataType = "Map", paramType = "path")
public R<List<TUserAccountReceiveAddress>> page(@RequestParam Map<String, Object> params){

    params.put(CommonConstant.DEL_FLAG,CommonConstant.STATUS_NORMAL);

    logger.info("查询参数：{}",ConvertMapUtil.convertMapOfUnderLine(params).keySet().toString());
    return new R<>(tUserAccountReceiveAddressService.selectByMap(ConvertMapUtil.convertMapOfUnderLine(params)));
}



/**
 * 添加
 * @param  tUserAccountReceiveAddress 实体
 * @return success/false
 */
@PostMapping("/add")
@ApiOperation(value = "新增实体 参数tUserAccountReceiveAddress实体", notes = "TUserAccountReceiveAddress新增方法")
@ApiImplicitParam(name = "tUserAccountReceiveAddress", value = "TUserAccountReceiveAddress实体", required = true, dataType = "TUserAccountReceiveAddress", paramType = "path")
public R<Boolean> add(@RequestBody TUserAccountReceiveAddress tUserAccountReceiveAddress){
    if("0".equals(tUserAccountReceiveAddress.getIsDefault())){
        List<TUserAccountReceiveAddress> list = tUserAccountReceiveAddressService.selectList(Condition.create().eq("account_id",tUserAccountReceiveAddress.getAccountId()));
        list.forEach(address->{
            address.setIsDefault("1");
        });
        tUserAccountReceiveAddressService.updateBatchById(list);
    }
    return new R<>(tUserAccountReceiveAddressService.insert(tUserAccountReceiveAddress));
}

/**
 * 删除
 * @param id ID
 * @return success/false
 */
@DeleteMapping("/{id}")
@ApiOperation(value = "按id删除", notes = "TUserAccountReceiveAddress删除")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<Boolean> delete(@PathVariable String id){return new R<>(tUserAccountReceiveAddressService.deleteById(id));}

/**
 * 编辑
 * @param  tUserAccountReceiveAddress 实体
 * @return success/false
 */
@PutMapping("/update")
@ApiOperation(value = "修改 参数tUserAccountReceiveAddress实体", notes = "TUserAccountReceiveAddress修改")
@ApiImplicitParam(name = "tUserAccountReceiveAddress", value = "TUserAccountReceiveAddress实体", required = true, dataType = "TUserAccountReceiveAddress", paramType = "path")
public R<Boolean> edit(@RequestBody TUserAccountReceiveAddress tUserAccountReceiveAddress){

    if("0".equals(tUserAccountReceiveAddress.getIsDefault())){
        List<TUserAccountReceiveAddress> list = tUserAccountReceiveAddressService.selectList(Condition.create()
                .eq("account_id",tUserAccountReceiveAddress.getAccountId())
        );
        list.forEach(address->{
            address.setIsDefault("1");
        });
        tUserAccountReceiveAddressService.updateBatchById(list);
    }
    tUserAccountReceiveAddress.setUpdateTime(new Date());
    return new R<>(tUserAccountReceiveAddressService.updateById(tUserAccountReceiveAddress));
}


}

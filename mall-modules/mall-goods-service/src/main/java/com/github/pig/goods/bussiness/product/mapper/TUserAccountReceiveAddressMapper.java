package com.github.pig.goods.bussiness.product.mapper;

import com.github.pig.goods.bussiness.product.entity.TUserAccountReceiveAddress;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户收货地址表 Mapper 接口
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
public interface TUserAccountReceiveAddressMapper extends BaseMapper<TUserAccountReceiveAddress> {

}

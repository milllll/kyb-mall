package com.github.pig.goods.bussiness.product.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pig.goods.bussiness.product.entity.TOrderProduct;
import com.github.pig.goods.bussiness.product.mapper.TOrderProductMapper;
import com.github.pig.goods.bussiness.product.service.ITOrderProductService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单产品关联表 服务实现类
 * </p>
 *
 * @author tao123
 * @since 2018-09-13
 */
@Service
public class TOrderProductServiceImpl extends ServiceImpl<TOrderProductMapper, TOrderProduct> implements ITOrderProductService {

}

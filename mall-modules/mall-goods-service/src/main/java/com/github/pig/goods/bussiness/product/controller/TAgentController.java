package com.github.pig.goods.bussiness.product.controller;
import com.baomidou.mybatisplus.mapper.Condition;
import com.github.pig.common.util.ConvertMapUtil;
import com.github.pig.common.util.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;


import com.github.pig.common.constant.CommonConstant;
import com.github.pig.common.util.Query;
import com.github.pig.common.util.R;
import com.github.pig.common.web.BaseController;
import com.github.pig.goods.bussiness.product.service.ITAgentService;
import com.github.pig.goods.bussiness.product.entity.TAgent;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 代理商表 前端控制器
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
@Api(value = "TAgentController", description = "代理商")
@RestController
@RequestMapping("/tAgent")
public class TAgentController extends BaseController{


@Autowired
private ITAgentService tAgentService;

/**
 * 通过ID查询
 *
 * @param id ID
 * @return TAgent
 */
@GetMapping("/{id}")
@ApiOperation(value = "  按id查询", httpMethod = "GET", notes = "  按id查询")
public R<TAgent> get(@PathVariable String id){
    return new R<>(tAgentService.selectById(id));
}


/**
* 分页查询信息
*
* @param params 分页对象
* @return 分页对象
*/
@RequestMapping("/page")
@ApiOperation(value = "  分页查询 参数为TAgent表中的属性 ", httpMethod = "GET", notes = "  分页查询")
public Page page(@RequestParam Map<String, Object> params){
    params = ConvertMapUtil.convertMapOfUnderLine(params);
    params.put(CommonConstant.DEL_FLAG,CommonConstant.STATUS_NORMAL);
    return tAgentService.selectPage(new Query<>(params),new EntityWrapper<>());
}

/**
 * 添加
 * @param  tAgent 实体
 * @return success/false
 */
@PostMapping("/add")
@ApiOperation(value = "  新增", httpMethod = "POST", notes = " 新增")
public R<Boolean> add(@RequestBody TAgent tAgent){
        if(tAgent.getAccountId()!=null&&tAgent.getAccountId()!=""){
            TAgent a = tAgentService.selectOne(Condition.create().eq("account_id",tAgent.getAccountId()));
            if (a != null) {
                return new R<>(false,"改用户已经是代理商了");
            }else{
                tAgent.setCreateTime(new Date());
                tAgent.setCreator(UserUtils.getUser());
                tAgent.setDelFlag(Integer.valueOf(CommonConstant.STATUS_NORMAL));
                return new R<>(tAgentService.insert(tAgent));
            }
        }else{
            return new R<>(false,"参数错误，用户编号不可为空！");
        }



}

/**
 * 删除
 * @param id ID
 * @return success/false
 */
@DeleteMapping("/{id}")
@ApiOperation(value = "  按id删除", notes = " 按id删除")
public R<Boolean> delete(@PathVariable String id){return new R<>(tAgentService.deleteById(id));}

/**
 * 编辑
 * @param  tAgent 实体
 * @return success/false
 */
@PutMapping("/update")
@ApiOperation(value = "  修改", notes = " 修改")
public R<Boolean> edit(@RequestBody TAgent tAgent){
    if (tAgent.getSn() == null) {
        return new R<>(false,"请修改代理商编号");
    }else {
       List<TAgent> a = tAgentService.selectList(Condition.create().eq("sn",tAgent.getSn()));
        if (a.size() > 1 ) {
            return new R<>(false,"该代理商编号已存在");
        }else{
            tAgent.setUpdateTime(new Date());
            tAgent.setEditer(UserUtils.getUser());
            return new R<>(tAgentService.updateById(tAgent));
        }
    }


}


}

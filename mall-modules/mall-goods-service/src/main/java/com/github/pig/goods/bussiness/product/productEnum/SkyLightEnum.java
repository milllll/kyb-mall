package com.github.pig.goods.bussiness.product.productEnum;

import com.baomidou.mybatisplus.enums.IEnum;

/**
 * <p>
 *  天窗枚举类
 * </p>
 * @Author: Carlos
 * @Date:2018/11/29 - 2:31 PM
 */
public enum  SkyLightEnum{


    LOCK("锁定", 1, "status"), UNLOCK("解锁", 0 ,"status");


    private String name;

    private int index;

    private String column;

    // 构造方法
    private SkyLightEnum(String name, int index,String column) {
        this.name = name;
        this.index = index;
        this.column = column;
    }


    /**
     * <p>
     *  重写toString方法
     * </p>
     * @Author: Carlos
     * @param: null
     * @return: index
     * @Date:2018/11/29 - 3:07 PM
     */
    @Override
    public String toString() {
        return this.index+":"+this.name;
    }

    /**
     * <p>
     *  获取对应数值
     * </p>
     * @Author: Carlos
     * @param: null
     * @return: index
     * @Date:2018/11/29 - 3:07 PM
     */
    public Integer getIndex(){
        return this.index;
    }


    /**
     * <p>
     *  获取数据库对应字段
     * </p>
     * @Author: Carlos
     * @param: null
     * @return: index
     * @Date:2018/11/29 - 3:07 PM
     */
    public String getColumn(){
        return this.column;
    }

}


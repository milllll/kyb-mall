package com.github.pig.goods.bussiness.product.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.github.pig.common.util.Query;
import com.github.pig.goods.bussiness.product.entity.TShopCar;
import com.github.pig.goods.bussiness.product.mapper.TShopCarMapper;
import com.github.pig.goods.bussiness.product.service.ITShopCarService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 购物车 服务实现类
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
@Service
public class TShopCarServiceImpl extends ServiceImpl<TShopCarMapper, TShopCar> implements ITShopCarService {



    @Autowired
    private TShopCarMapper tShopCarMapper;

    /**
     * <p>
     *  分页查询购物车
     * </p>
     * @Author: Carlos
     * @param:
     * @return:
     * @Date:2019/3/14 - 4:17 PM
     */
    @Override
    public Page selectWithShopCarPage(Query query) {
        query.setRecords(tShopCarMapper.selectShopCarPage(query,query.getCondition()));
        return query;
    }
}

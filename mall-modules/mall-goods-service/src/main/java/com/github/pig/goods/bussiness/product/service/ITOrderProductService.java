package com.github.pig.goods.bussiness.product.service;

import com.baomidou.mybatisplus.service.IService;
import com.github.pig.goods.bussiness.product.entity.TOrderProduct;

/**
 * <p>
 * 订单产品关联表 服务类
 * </p>
 *
 * @author tao123
 * @since 2018-09-13
 */
public interface ITOrderProductService extends IService<TOrderProduct> {

}

package com.github.pig.goods.bussiness.product.service.impl;

import com.github.pig.goods.bussiness.product.entity.TTopic;
import com.github.pig.goods.bussiness.product.mapper.TTopicMapper;
import com.github.pig.goods.bussiness.product.service.ITTopicService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 主题表 服务实现类
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
@Service
public class TTopicServiceImpl extends ServiceImpl<TTopicMapper, TTopic> implements ITTopicService {

}

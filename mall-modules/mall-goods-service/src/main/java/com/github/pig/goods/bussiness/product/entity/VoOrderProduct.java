package com.github.pig.goods.bussiness.product.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 *  订单产品视图
 * </p>
 * @Author: Carlos
 * @Date:2019/4/21 - 10:41 PM
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VoOrderProduct extends TOrderProduct{

    private  TProduct product;
}

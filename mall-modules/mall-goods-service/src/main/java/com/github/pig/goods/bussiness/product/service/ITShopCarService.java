package com.github.pig.goods.bussiness.product.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.github.pig.common.util.Query;
import com.github.pig.goods.bussiness.product.entity.TShopCar;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 购物车 服务类
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
public interface ITShopCarService extends IService<TShopCar> {

    /**
     * 分页查询产品信息
     *
     * @param query  查询条件
     * @return Page 分页
     */
    Page selectWithShopCarPage(Query query);

}

package com.github.pig.goods.bussiness.product.entity;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户收货地址表 false
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
@Data
@EqualsAndHashCode()
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Builder
@TableName("t_user_account_receive_address")
public class TUserAccountReceiveAddress extends Model<TUserAccountReceiveAddress> {



    /**
     * 主键
     */

    @TableId(value = "id", type = IdType.UUID)
    @TableField("id")
    private String id;
    /**
     * 地址对应用户ID
     */

    @TableField("account_id")
    private String accountId;
    /**
     * 收货人姓名
     */

    @TableField("consignee_name")
    private String consigneeName;
    /**
     * 手机号码
     */

    @TableField("phone_number")
    private String phoneNumber;
    /**
     * 省
     */

    @TableField("province")
    private Long province;
    /**
     * 省份名称
     */

    @TableField("province_name")
    private String provinceName;
    /**
     * 市
     */

    @TableField("city")
    private Long city;
    /**
     * 城市名称
     */

    @TableField("city_name")
    private String cityName;
    /**
     * 县/区
     */

    @TableField("district")
    private Long district;
    /**
     * 县/区
     */

    @TableField("district_name")
    private String districtName;
    /**
     * 详细地址
     */

    @TableField("address")
    private String address;
    /**
     * 全地址
     */

    @TableField("full_address")
    private String fullAddress;
    /**
     * 是否默认地址，0-默认地址,1-非默认地址
     */

    @TableField("is_default")
    private String isDefault;
    /**
     * 创建时间
     */

    @TableField("create_time")
    private Date createTime;
    /**
     * 修改时间
     */

    @TableField("update_time")
    private Date updateTime;
    /**
     * 标识是否删除:0-未删除，1-删除
     */

    @TableField("is_deleted")
    private Integer isDeleted;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}

package com.github.pig.goods.bussiness.product.mapper;

import com.github.pig.goods.bussiness.product.entity.TProductPrice;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 产品价格表 Mapper 接口
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
public interface TProductPriceMapper extends BaseMapper<TProductPrice> {

}

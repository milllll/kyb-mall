package com.github.pig.goods.Configation;

import com.github.pig.goods.common.util.JsonUtils;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * wechat mp properties
 *
 * @author Binary Wang(https://github.com/binarywang)
 */
@Data
@ConfigurationProperties(prefix = "wx.mp")
public class WxMpProperties {

    private List<MpConfig> configs;

    @Data
    public static class MpConfig {
        /**
         * 设置微信公众号的appid
         */
        private String appId = "wx6178dfa0adaf26f9";

        /**
         * 设置微信公众号的app secret
         */
        private String secret = "f57bd8a8625ac2b91cb1b56f01496c21";

        /**
         * 设置微信公众号的token
         */
        private String token= "kybwxmall";

        /**
         * 设置微信公众号的EncodingAESKey
         *    appId: wx6178dfa0adaf26f9
         *       secret: f57bd8a8625ac2b91cb1b56f01496c21）
         *       token: kybwxmall
         *       aesKey: 1IbIokSfLozJeqKeQjN2WFJm5khVSYqC34xWQeRi6pM
         *   pay:
         */
        private String aesKey ="1IbIokSfLozJeqKeQjN2WFJm5khVSYqC34xWQeRi6pM";



    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }
}

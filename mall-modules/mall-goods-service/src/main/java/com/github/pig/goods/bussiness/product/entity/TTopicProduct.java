package com.github.pig.goods.bussiness.product.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 主题产品关联表 false
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
@Data
@EqualsAndHashCode()
@Accessors(chain = true)
@Builder
@TableName("t_topic_product")
public class TTopicProduct extends Model<TTopicProduct> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */

    @TableId(value = "id", type = IdType.AUTO)
    @TableField("id")
    private String id;
    /**
     * topicID
     */

    @TableField("topic_id")
    private String topicId;
    /**
     * 产品ID
     */

    @TableField("product_id")
    private String productId;
    /**
     * 产品顺序
     */

    @TableField("sort")
    private Long sort;
    /**
     * 状态
     */

    @TableField("status")
    private String status;

    /**
     * 更新时间
     */

    @TableField("update_time")
    private Date updateTime;

    public TTopicProduct(String id, String topicId, String productId, Long sort, String status, Date updateTime) {

        this.id = id;
        this.topicId = topicId;
        this.productId = productId;
        this.sort = sort;
        this.status = status;
        this.updateTime = updateTime;
    }

    public TTopicProduct() { }
    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}

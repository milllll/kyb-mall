package com.github.pig.goods.common.util.pay;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WxPay{
    private String paySign;
    private String prepayId;
    private String nonceStr;
    private String timeStamp;

}

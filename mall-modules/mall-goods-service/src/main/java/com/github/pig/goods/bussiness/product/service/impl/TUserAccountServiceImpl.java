package com.github.pig.goods.bussiness.product.service.impl;

import com.github.pig.goods.bussiness.product.entity.TUserAccount;
import com.github.pig.goods.bussiness.product.mapper.TUserAccountMapper;
import com.github.pig.goods.bussiness.product.service.ITUserAccountService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
@Service
public class TUserAccountServiceImpl extends ServiceImpl<TUserAccountMapper, TUserAccount> implements ITUserAccountService {

}

package com.github.pig.goods.bussiness.product.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;


import com.github.pig.common.constant.CommonConstant;
import com.github.pig.common.util.Query;
import com.github.pig.common.util.R;
import com.github.pig.common.web.BaseController;
import com.github.pig.goods.bussiness.product.service.ITTopicService;
import com.github.pig.goods.bussiness.product.entity.TTopic;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;

import java.util.Date;
import java.util.Map;

/**
 * <p>
 * 主题表 前端控制器
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
@RestController
@RequestMapping("/tTopic")
@Api(value = "TTopicController", description = "主题表")
public class TTopicController extends BaseController{


@Autowired
private ITTopicService tTopicService;

/**
 * 通过ID查询
 *
 * @param id ID
 * @return TTopic
 */
@GetMapping("/{id}")
@ApiOperation(value = "  按id查询", httpMethod = "GET", notes = "  按id查询")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<TTopic> get(@PathVariable String id){
    return new R<>(tTopicService.selectById(id));
}


/**
* 分页查询信息
*
* @param params 分页对象
* @return 分页对象
*/
@RequestMapping("/page")
@ApiOperation(value = "分页查询", httpMethod = "GET",notes = "TTopic列表方法")
@ApiImplicitParam(name = "查询参数", value = "TTopic参数", required = true, dataType = "Map", paramType = "path")
public Page page(@RequestParam Map<String, Object> params){
    params.put(CommonConstant.DEL_FLAG,CommonConstant.STATUS_NORMAL);
    return tTopicService.selectPage(new Query<>(params),new EntityWrapper<>());
}

/**
 * 添加
 * @param  tTopic 实体
 * @return success/false
 */
@PostMapping("/add")
@ApiOperation(value = "新增实体", notes = "TTopic新增方法")
@ApiImplicitParam(name = "tTopic", value = "TTopic实体", required = true, dataType = "TTopic", paramType = "path")
public R<Boolean> add( TTopic tTopic){
    return new R<>(tTopicService.insert(tTopic));
}

/**
 * 删除
 * @param id ID
 * @return success/false
 */
@DeleteMapping("/{id}")
@ApiOperation(value = "删除", notes = "TTopic删除")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<Boolean> delete(@PathVariable String id){return new R<>(tTopicService.deleteById(id));}

/**
 * 编辑
 * @param  tTopic 实体
 * @return success/false
 */
@PutMapping("/update")
@ApiOperation(value = "修改", notes = "TTopic修改")
@ApiImplicitParam(name = "tTopic", value = "TTopic实体", required = true, dataType = "TTopic", paramType = "path")
public R<Boolean> edit(@RequestBody TTopic tTopic){
    tTopic.setUpdateTime(new Date());
    return new R<>(tTopicService.updateById(tTopic));
}


}

package com.github.pig.goods.bussiness.product.controller;
import com.baomidou.mybatisplus.mapper.Condition;
import com.github.pig.goods.bussiness.product.entity.TProduct;
import com.github.pig.goods.bussiness.product.service.ITProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;


import com.github.pig.common.constant.CommonConstant;
import com.github.pig.common.util.Query;
import com.github.pig.common.util.R;
import com.github.pig.common.web.BaseController;
import com.github.pig.goods.bussiness.product.service.ITProductPriceService;
import com.github.pig.goods.bussiness.product.entity.TProductPrice;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 产品价格表 前端控制器
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
@RestController
@RequestMapping("/tProductPrice")
@Slf4j
@Api(value = "TProductPriceController", description = "产品价格表")
public class TProductPriceController extends BaseController{


@Autowired
private ITProductPriceService tProductPriceService;

@Autowired
private ITProductService tProductService;


/**
 * 通过ID查询
 *
 * @param id ID
 * @return TProductPrice
 */
@GetMapping("/{id}")
@ApiOperation(value = "  按id查询", httpMethod = "GET", notes = "  按id查询")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<TProductPrice> get(@PathVariable String id){
    return new R<>(tProductPriceService.selectById(id));
}


/**
* 分页查询信息
*
* @param params 分页对象
* @return 分页对象
*/
@RequestMapping("/page")
@ApiOperation(value = "分页查询 参数为TProductPrice表中的属性",httpMethod = "GET", notes = "TProductPrice列表方法")
@ApiImplicitParam(name = "查询参数", value = "TProductPrice参数", required = true, dataType = "Map", paramType = "path")
public Page page(@RequestParam Map<String, Object> params){
    params.put(CommonConstant.DEL_FLAG,CommonConstant.STATUS_NORMAL);
    return tProductPriceService.selectPage(new Query<>(params),new EntityWrapper<>());
}

/**
 * 添加
 * @param  tProductPrice 实体
 * @return success/false
 */
@PostMapping("/add")
@ApiOperation(value = "新增实体 参数为tProductPrice实体", notes = "TProductPrice新增方法")
@ApiImplicitParam(name = "tProductPrice", value = "TProductPrice实体", required = true, dataType = "TProductPrice", paramType = "path")
public R<Boolean> add( TProductPrice tProductPrice){
    log.info("产品新增定价开始------");
    boolean delTag =tProductPriceService.delete(Condition.create().eq("product_id",tProductPrice.getProductId()));
    log.info("删除原定价：{}",delTag);
    boolean tag =  tProductPriceService.insert(tProductPrice);
    log.info("定价:{}",tag);
    if (tag) {
        boolean updateTag =tProductService.updateById(TProduct.builder().id(tProductPrice.getProductId()).isHasPrice(1).build());
        log.info("修改状态:{}",updateTag);
    }
    return new R<>(tag);
}



/**
 * 添加
 * @param  tProductPriceList 批量定价
 * @return success/false
 */
@PostMapping("/addBatch")
@ApiOperation(value = "新增实体 参数为tProductPrice实体", notes = "TProductPrice新增方法")
public R<Boolean> add(@RequestBody  List<TProductPrice> tProductPriceList){
    log.info("批量定价接口开始----------");
    boolean delTag =tProductPriceService.delete(Condition.create().eq("product_id",tProductPriceList.get(0).getProductId()));
    log.info("删除原定价：{}",delTag);
    boolean insertTag =tProductPriceService.insertBatch(tProductPriceList);
    log.info("批量定价:{}",insertTag);
    if (insertTag) {
        List<TProduct> productList = tProductPriceList.stream().map(price->TProduct.builder().id(price.getProductId()).isHasPrice(1).build()).collect(Collectors.toList());
        boolean updateTag = tProductService.updateBatchById(productList);
        log.info("修改产品 标示:{}",updateTag);
    }
    return new R<>(insertTag);
}



/**
 * 删除
 * @param id ID
 * @return success/false
 */
@DeleteMapping("/{id}")
@ApiOperation(value = "按id删除", notes = "TProductPrice删除")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<Boolean> delete(@PathVariable String id){return new R<>(tProductPriceService.deleteById(id));}

/**
 * 编辑
 * @param  tProductPrice 实体
 * @return success/false
 */
@PutMapping("/update")
@ApiOperation(value = "修改 tProductPrice", notes = "TProductPrice修改")
@ApiImplicitParam(name = "tProductPrice", value = "TProductPrice实体", required = true, dataType = "TProductPrice", paramType = "path")
public R<Boolean> edit(@RequestBody TProductPrice tProductPrice){
    tProductPrice.setUpdateTime(new Date());
    return new R<>(tProductPriceService.updateById(tProductPrice));
}


}

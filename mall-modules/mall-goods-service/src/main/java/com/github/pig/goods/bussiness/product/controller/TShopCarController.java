package com.github.pig.goods.bussiness.product.controller;
import com.baomidou.mybatisplus.mapper.Condition;
import com.github.pig.common.util.ConvertMapUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;


import com.github.pig.common.constant.CommonConstant;
import com.github.pig.common.util.Query;
import com.github.pig.common.util.R;
import com.github.pig.common.web.BaseController;
import com.github.pig.goods.bussiness.product.service.ITShopCarService;
import com.github.pig.goods.bussiness.product.entity.TShopCar;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 购物车 前端控制器
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
@RestController
@RequestMapping("/tShopCar")
@Api(value = "TShopCarController", description = "购物车")
public class TShopCarController extends BaseController{


@Autowired
private ITShopCarService tShopCarService;

/**
 * 通过ID查询
 *
 * @param id ID
 * @return TShopCar
 */
@GetMapping("/{id}")
@ApiOperation(value = "  按id查询", httpMethod = "GET", notes = "  按id查询")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<TShopCar> get(@PathVariable String id){
    return new R<>(tShopCarService.selectById(id));
}


/**
* 分页查询信息
*
* @param params 分页对象
* @return 分页对象
*/
@RequestMapping("/page")
@ApiOperation(value = "分页查询",httpMethod = "GET", notes = "TShopCar列表方法")
@ApiImplicitParam(name = "查询参数", value = "TShopCar参数", required = true, dataType = "Map", paramType = "path")
public Page page(@RequestParam Map<String, Object> params){

    params.put(CommonConstant.DEL_FLAG,CommonConstant.STATUS_NORMAL);
    return tShopCarService.selectWithShopCarPage(new Query<>(params));
}

/**
 * 查询信息列表
 *
 * @param params 分页对象
 * @return 分页对象
 */
@RequestMapping("/list")
@ApiOperation(value = "列表查询",httpMethod = "GET", notes = "TShopCar列表方法")
@ApiImplicitParam(name = "查询参数", value = "TShopCar参数", required = true, dataType = "Map", paramType = "path")
public List<TShopCar> list(@RequestParam Map<String, Object> params){
    params.put(CommonConstant.DEL_FLAG,CommonConstant.STATUS_NORMAL);
    return tShopCarService.selectByMap(ConvertMapUtil.convertMapOfUnderLine(params));
}


/**
 * 添加
 * @param  tShopCar 实体
 * @return success/false
 */
@PostMapping("/add")
@ApiOperation(value = "新增实体", notes = "TShopCar新增方法")
@ApiImplicitParam(name = "tShopCar", value = "TShopCar实体", required = true, dataType = "TShopCar", paramType = "path")
public R<Boolean> add(@RequestBody TShopCar tShopCar){

    TShopCar newCar = tShopCarService.selectOne(Condition.create()
            .eq("account_id",tShopCar.getAccountId())
            .and()
            .eq("product_id",tShopCar.getProductId()));

    if (newCar != null) {
        newCar.setAmount(newCar.getAmount()+tShopCar.getAmount());
        logger.info("该商品之前添加过购物车，商品id:{},累加后的数量为：{}",
                newCar.getProductId(),newCar.getAmount());
        return new R<>(tShopCarService.updateById(newCar));
    }else{

        return new R<>(tShopCarService.insert(tShopCar));
    }
}

/**
 * 删除
 * @param id ID
 * @return success/false
 */
@DeleteMapping("/{id}")
@ApiOperation(value = "删除", notes = "TShopCar删除")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<Boolean> delete(@PathVariable String id){return new R<>(tShopCarService.deleteById(id));}

/**
 * 编辑
 * @param  tShopCar 实体
 * @return success/false
 */
@PutMapping("/update")
@ApiOperation(value = "修改", notes = "TShopCar修改")
@ApiImplicitParam(name = "tShopCar", value = "TShopCar实体", required = true, dataType = "TShopCar", paramType = "path")
public R<Boolean> edit(@RequestBody TShopCar tShopCar){
    tShopCar.setUpdateTime(new Date());
    return new R<>(tShopCarService.updateById(tShopCar));
}


}

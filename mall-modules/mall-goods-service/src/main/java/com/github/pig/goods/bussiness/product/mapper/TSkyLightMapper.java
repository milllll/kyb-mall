package com.github.pig.goods.bussiness.product.mapper;

import com.github.pig.goods.bussiness.product.entity.TSkyLight;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 天窗（轮播图） Mapper 接口
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
public interface TSkyLightMapper extends BaseMapper<TSkyLight> {

}

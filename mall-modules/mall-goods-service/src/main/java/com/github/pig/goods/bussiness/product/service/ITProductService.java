package com.github.pig.goods.bussiness.product.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.github.pig.common.util.Query;
import com.github.pig.common.vo.UserVO;
import com.github.pig.goods.bussiness.product.entity.TProduct;
import com.baomidou.mybatisplus.service.IService;
import com.github.pig.goods.bussiness.product.entity.ToProduct;

/**
 * <p>
 * 产品表 服务类
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-23
 */
public interface ITProductService extends IService<TProduct> {


    /**
     * 分页查询产品信息
     *
     * @param query  查询条件
     * @return Page 分页
     */
    Page selectWithProductPage(Query query);


    /**
     * 通过ID查询产品信息
     *
     * @param id 用户ID
     * @return ToProduct
     */
    ToProduct getProductById(String id);

}

package com.github.pig.goods.bussiness.product.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 购物车 false
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
@Data
@EqualsAndHashCode()
@Accessors(chain = true)
@Builder
@TableName("t_shop_car")
public class TShopCar extends Model<TShopCar> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */

    @TableId(value = "id", type = IdType.UUID)
    @TableField("id")
    private String id;
    /**
     * 产品ID
     */

    @TableField("product_id")
    private String productId;
    /**
     * 用户ID
     */

    @TableField("account_id")
    private String accountId;
    /**
     * 数量
     */

    @TableField("amount")
    private Float amount;
    /**
     * 单位
     */

    @TableField("unit")
    private String unit;
    /**
     * 代理商ID
     */

    @TableField("agent_id")
    private String agentId;
    /**
     * 创建时间
     */

    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */

    @TableField("update_time")
    private Date updateTime;

    public TShopCar(String id, String productId, String accountId, Float amount, String unit, String agentId, Date createTime, Date updateTime) {
        this.id = id;
        this.productId = productId;
        this.accountId = accountId;
        this.amount = amount;
        this.unit = unit;
        this.agentId = agentId;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public TShopCar() { }
    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}

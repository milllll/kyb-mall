package com.github.pig.goods.bussiness.product.service;

import com.github.pig.goods.bussiness.product.entity.TTopic;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 主题表 服务类
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
public interface ITTopicService extends IService<TTopic> {

}

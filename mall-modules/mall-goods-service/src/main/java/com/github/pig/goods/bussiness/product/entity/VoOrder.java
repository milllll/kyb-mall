package com.github.pig.goods.bussiness.product.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  订单对象视图
 * </p>
 * @Author: Carlos
 * @Date:2018/11/2 - 10:01 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VoOrder extends TOrder {

    private List<TOrderProduct> productList;

    private TUserAccountReceiveAddress address;

    private TAgent agent;

    private TUserAccount account;

}

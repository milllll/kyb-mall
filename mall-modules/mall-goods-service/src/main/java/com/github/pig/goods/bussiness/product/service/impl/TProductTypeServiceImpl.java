package com.github.pig.goods.bussiness.product.service.impl;

import com.github.pig.goods.bussiness.product.entity.TProductType;
import com.github.pig.goods.bussiness.product.mapper.TProductTypeMapper;
import com.github.pig.goods.bussiness.product.service.ITProductTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 产品类型 服务实现类
 * </p>
 *
 * @author Carlos123
 * @since 2018-11-22
 */
@Service
public class TProductTypeServiceImpl extends ServiceImpl<TProductTypeMapper, TProductType> implements ITProductTypeService {

}

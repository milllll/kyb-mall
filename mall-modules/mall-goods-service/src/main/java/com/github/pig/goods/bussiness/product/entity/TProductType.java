package com.github.pig.goods.bussiness.product.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 字典表 false
 * </p>
 *
 * @author Carlos123
 * @since 2018-11-22
 */
@Data
@EqualsAndHashCode()
@Accessors(chain = true)
@AllArgsConstructor()
@NoArgsConstructor()
@Builder
@TableName("t_product_type")
public class TProductType extends Model<TProductType> {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */

    @TableId(value = "id", type = IdType.UUID)
    @TableField("id")
    private String id;
    /**
     * 数据值
     */

    @TableField("value")
    private String value;
    /**
     * 标签名
     */

    @TableField("label")
    private String label;
    /**
     * 类型
     */

    @TableField("type")
    private String type;
    /**
     * 图片地址
     */

    @TableField("image")
    private String image;
    /**
     * 描述
     */

    @TableField("description")
    private String description;
    /**
     * 排序（升序）
     */

    @TableField("sort")
    private BigDecimal sort;
    /**
     * 父签名
     */

    @TableField("parent_label")
    private String parentLabel;
    /**
     * 层级
     */

    @TableField("level")
    private Integer level;
    /**
     * 创建时间
     */

    @TableField("create_time")
    private Date createTime;
    /**
     * 更新时间
     */

    @TableField("update_time")
    private Date updateTime;
    /**
     * 备注信息
     */

    @TableField("remarks")
    private String remarks;
    /**
     * 创建人
     */

    @TableField("creator")
    private String creator;
    /**
     * 更新人
     */

    @TableField("editor")
    private String editor;
    /**
     * 删除标记
     */

    @TableField("del_flag")
    private String delFlag;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}



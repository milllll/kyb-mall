package com.github.pig.goods.bussiness.product.service.impl;

import com.github.pig.goods.bussiness.product.entity.TAgent;
import com.github.pig.goods.bussiness.product.mapper.TAgentMapper;
import com.github.pig.goods.bussiness.product.service.ITAgentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 代理商表 服务实现类
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
@Service
public class TAgentServiceImpl extends ServiceImpl<TAgentMapper, TAgent> implements ITAgentService {

}

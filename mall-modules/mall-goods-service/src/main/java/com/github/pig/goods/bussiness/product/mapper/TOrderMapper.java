package com.github.pig.goods.bussiness.product.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.github.pig.common.util.Query;
import com.github.pig.goods.bussiness.product.entity.TOrder;
import com.github.pig.goods.bussiness.product.entity.VoOrder;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单主表 Mapper 接口
 * </p>
 *
 * @author tao123
 * @since 2018-09-13
 */
public interface TOrderMapper extends BaseMapper<TOrder> {

    /**
     * <p>
     * selectOrderPage
     * </p>
     * @Author: Carlos
     * @param: query  query
     * @param: map select map
     * @return:java.util.List
     * @Date:2018/11/2 - 10:04 PM
     */
    List selectOrderPage(Query query, @RequestParam Map map);

    /**
     * <p>
     * selectOrderForMobile
     * </p>
     * @Author: Carlos
     * @param: query  query
     * @param: map select map
     * @return:java.util.List
     * @Date:2018/11/2 - 10:04 PM
     */
    List selectOrderForMobile(Query query, @RequestParam Map map);


    /**
     * 通过ID查询产品信息
     *
     * @param id 用户ID
     * @return ToProduct
     */
    VoOrder selectOrderById(@Param(value="id") String id);

}

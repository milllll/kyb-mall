package com.github.pig.goods.bussiness.product.mapper;

import com.github.pig.common.bean.interceptor.DataScope;
import com.github.pig.common.util.Query;
import com.github.pig.goods.bussiness.product.entity.TProduct;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.github.pig.goods.bussiness.product.entity.ToProduct;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 产品表 Mapper 接口
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-23
 */
public interface TProductMapper extends BaseMapper<TProduct> {


    /**
     * 分页查询产品信息
     *
     * @param query     查询条件
     * @return list
     */
    List selectProductPage(Query query, @RequestParam Map map);

    /**
     * 通过ID查询产品信息
     *
     * @param id 用户ID
     * @return ToProduct
     */
    ToProduct getProductById(@Param(value="id") String id);
}

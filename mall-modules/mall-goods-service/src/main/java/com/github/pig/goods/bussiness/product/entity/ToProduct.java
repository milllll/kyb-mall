package com.github.pig.goods.bussiness.product.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 产品扩展类
 * </p>
 * @Author: Carlos
 * @Date:2018/9/25 - 下午10:22
 */
@Data
@AllArgsConstructor()
@NoArgsConstructor()
public class ToProduct extends TProduct {

    List<TProductPrice> priceList;





}

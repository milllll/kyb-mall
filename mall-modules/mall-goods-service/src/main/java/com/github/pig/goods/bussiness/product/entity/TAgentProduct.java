package com.github.pig.goods.bussiness.product.entity;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 * 代理商产品关联表 false
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
@Data
@EqualsAndHashCode()
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Builder
@TableName("t_agent_product")
public class TAgentProduct extends Model<TAgentProduct> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */

    @TableId(value = "id", type = IdType.UUID)
    @TableField("id")
    private String id;
    /**
     * 代理商ID
     */

    @TableField("agent_id")
    private String agentId;
    /**
     * 产品ID
     */

    @TableField("product_id")
    private String productId;
    /**
     * 加密后的码值
     */

    @TableField("sign")
    private String sign;
    /**
     * 创建时间
     */

    @TableField("create_time")
    private Date createTime;
    /**
     * 创建者
     */

    @TableField("creator")
    private String creator;

    @TableField("del_flag")
    private Integer delFlag = 0;

    @TableField("qr")
    private String qr;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}

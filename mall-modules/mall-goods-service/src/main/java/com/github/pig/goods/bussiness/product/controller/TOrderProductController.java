package com.github.pig.goods.bussiness.product.controller;
import com.github.pig.goods.bussiness.product.entity.TOrderProduct;
import com.github.pig.goods.bussiness.product.service.ITOrderProductService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;


import com.github.pig.common.constant.CommonConstant;
import com.github.pig.common.util.Query;
import com.github.pig.common.util.R;
import com.github.pig.common.web.BaseController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;

import java.util.Date;
import java.util.Map;

/**
 * <p>
 * 订单产品关联表 前端控制器
 * </p>
 *
 * @author tao123
 * @since 2018-09-13
 */
@Api(value = "TOrderProductController", description = "订单产品关联表")
@RestController
@RequestMapping("/tOrderProduct")
public class TOrderProductController extends BaseController{


@Autowired
private ITOrderProductService tOrderProductService;

/**
 * 通过ID查询
 *
 * @param id ID
 * @return TOrderProduct
 */
@GetMapping("/{id}")
@ApiOperation(value = "  按id查询", httpMethod = "GET", notes = "  按id查询")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<TOrderProduct> get(@PathVariable long id){
    return new R<>(tOrderProductService.selectById(id));
}


/**
* 分页查询信息
*
* @param params 分页对象
* @return 分页对象
*/
@RequestMapping("/page")
@ApiOperation(value = "分页查询 参数为TAgentProduct表中的属性", notes = "TOrderProduct列表方法")
@ApiImplicitParam(name = "查询参数", value = "TOrderProduct参数", required = true, dataType = "Map", paramType = "path")
public Page page(@RequestParam Map<String, Object> params){
    params.put(CommonConstant.DEL_FLAG,CommonConstant.STATUS_NORMAL);
    return tOrderProductService.selectPage(new Query<>(params),new EntityWrapper<>());
}

/**
 * 添加
 * @param  tOrderProduct 实体
 * @return success/false
 */
@PostMapping("/add")
@ApiOperation(value = "新增实体 参数为tOrderProduct实体", notes = "TOrderProduct新增方法")
@ApiImplicitParam(name = "tOrderProduct", value = "TOrderProduct实体", required = true, dataType = "TOrderProduct", paramType = "path")
public R<Boolean> add( TOrderProduct tOrderProduct){
    return new R<>(tOrderProductService.insert(tOrderProduct));
}

/**
 * 删除
 * @param id ID
 * @return success/false
 */
@DeleteMapping("/{id}")
@ApiOperation(value = "按id删除", notes = "TOrderProduct删除")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<Boolean> delete(@PathVariable long id){return new R<>(tOrderProductService.deleteById(id));}

/**
 * 编辑
 * @param  tOrderProduct 实体
 * @return success/false
 */
@PutMapping("/update")
@ApiOperation(value = "修改 参数为tOrderProduct实体", notes = "TOrderProduct修改")
@ApiImplicitParam(name = "tOrderProduct", value = "TOrderProduct实体", required = true, dataType = "TOrderProduct", paramType = "path")
public R<Boolean> edit(@RequestBody TOrderProduct tOrderProduct){
    tOrderProduct.setUpdateTime(new Date());
    return new R<>(tOrderProductService.updateById(tOrderProduct));
}


}

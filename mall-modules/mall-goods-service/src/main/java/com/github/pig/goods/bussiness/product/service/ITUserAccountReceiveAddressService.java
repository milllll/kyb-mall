package com.github.pig.goods.bussiness.product.service;

import com.github.pig.goods.bussiness.product.entity.TUserAccountReceiveAddress;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户收货地址表 服务类
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
public interface ITUserAccountReceiveAddressService extends IService<TUserAccountReceiveAddress> {

}

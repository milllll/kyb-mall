package com.github.pig.goods.bussiness.product.mapper;

import com.github.pig.goods.bussiness.product.entity.TUserAccount;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
public interface TUserAccountMapper extends BaseMapper<TUserAccount> {

}

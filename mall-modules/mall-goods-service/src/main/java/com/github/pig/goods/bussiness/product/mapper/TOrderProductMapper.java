package com.github.pig.goods.bussiness.product.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.github.pig.goods.bussiness.product.entity.TOrderProduct;

/**
 * <p>
 * 订单产品关联表 Mapper 接口
 * </p>
 *
 * @author tao123
 * @since 2018-09-13
 */
public interface TOrderProductMapper extends BaseMapper<TOrderProduct> {

}

package com.github.pig.goods.bussiness.product.service;

import com.github.pig.goods.bussiness.product.entity.TAgent;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 代理商表 服务类
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
public interface ITAgentService extends IService<TAgent> {

}

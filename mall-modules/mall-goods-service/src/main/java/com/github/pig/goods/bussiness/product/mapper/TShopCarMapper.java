package com.github.pig.goods.bussiness.product.mapper;

import com.github.pig.common.util.Query;
import com.github.pig.goods.bussiness.product.entity.TShopCar;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 购物车 Mapper 接口
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
public interface TShopCarMapper extends BaseMapper<TShopCar> {


    /**
     * 分页查询购物车信息
     *
     * @param query     查询条件
     * @return list
     */
    List selectShopCarPage(Query query, @RequestParam Map map);

}

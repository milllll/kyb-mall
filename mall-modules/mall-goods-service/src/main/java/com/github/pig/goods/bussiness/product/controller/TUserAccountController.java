package com.github.pig.goods.bussiness.product.controller;
import com.baomidou.mybatisplus.mapper.Condition;
import com.github.pig.common.util.ConvertMapUtil;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;


import com.github.pig.common.constant.CommonConstant;
import com.github.pig.common.util.Query;
import com.github.pig.common.util.R;
import com.github.pig.common.web.BaseController;
import com.github.pig.goods.bussiness.product.service.ITUserAccountService;
import com.github.pig.goods.bussiness.product.entity.TUserAccount;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;

import javax.validation.Validation;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Map;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
@RestController
@RequestMapping("/tUserAccount")
@Api(value = "TUserAccountController", description = "用户表")
public class TUserAccountController extends BaseController{


@Autowired
private ITUserAccountService tUserAccountService;

/**
 * 通过ID查询
 *
 * @param id ID
 * @return TUserAccount
 */
@GetMapping("/{id}")
@ApiOperation(value = "  按id查询", httpMethod = "GET", notes = "  按id查询")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<TUserAccount> get(@PathVariable String id){
    return new R<>(tUserAccountService.selectById(id));
}

    /**
     * 登录
     *
     * @param phone
     * @param password
     * @param openId
     * @return TUserAccount
     */
    @GetMapping("/login")
    @ApiOperation(value = "app登录", httpMethod = "GET", notes = "app登录")
    @ApiImplicitParams({
    @ApiImplicitParam(name = "用户电话号码", value = "phone", required = true, dataType = "String", paramType = "path"),
    @ApiImplicitParam(name = "用户密码", value = "password", required = true, dataType = "String", paramType = "path"),
    @ApiImplicitParam(name = "用户openId", value = "openId", dataType = "String", paramType = "path"),
    })
    public R<TUserAccount> userLogin(@NotNull String phone, @NotNull String password , String openId){

        TUserAccount account = tUserAccountService.selectOne(Condition.create().eq("phone_number",phone));
        if (account != null) {
            logger.info(account.toString());
            if(password.equals(account.getPassword())){
                if(openId!=null){
                    account.setOpenId(openId);
                    account.setLatestLoginTime(new Date());
                    tUserAccountService.updateById(account);
                    return new R<>(account,"登录成功，已绑定openId和账户信息");
                }else{
                    return new R<>(account,"登录成功");
                }
            }else{
                return new R<>(null,"账号电话号码或密码不正确！");
            }
        }else {
            return new R<>(null,"账号电话号码或密码不正确 ！");
        }

    }



/**
* 分页查询信息
*
* @param params 分页对象
* @return 分页对象
*/
@RequestMapping("/page")
@ApiOperation(value = "分页查询 参数为TUserAccount表中的属性", httpMethod = "GET",notes = "TUserAccount列表方法")
@ApiImplicitParam(name = "查询参数", value = "TUserAccount参数", required = true, dataType = "Map", paramType = "path")
public Page page(@RequestParam Map<String, Object> params){
    params = ConvertMapUtil.convertMapOfUnderLine(params);
    params.put(CommonConstant.DEL_FLAG,CommonConstant.STATUS_NORMAL);
    if (params.get("startTime") != null||params.get("endTime") != null) {
        tUserAccountService.selectPage(new Query<>(params),new EntityWrapper<>(new TUserAccount())
                .between("create_time",
                        params.get("startTime")==null?new Date(2009,1,1):params.get("startTime"),
                        params.get("endTime")==new Date()?new Date():params.get("endTime")));
    }

    return tUserAccountService.selectPage(new Query<>(params),new EntityWrapper<>());
}

/**
 * 添加
 * @param  tUserAccount 实体
 * @return success/false
 */
@PostMapping("/add")
@ApiOperation(value = "注册用户", notes = "TUserAccount新增方法")
@ApiImplicitParam(name = "tUserAccount", value = "TUserAccount实体", required = true, dataType = "TUserAccount", paramType = "path")
public R<Boolean> add(@RequestBody  TUserAccount tUserAccount){
    tUserAccount.setCreateTime(new Date());
    tUserAccount.setDelFlag(0);
    tUserAccount.setStatus("0");

    return new R<>(tUserAccountService.insert(tUserAccount));
}

/**
 * 删除
 * @param id ID
 * @return success/false
 */
@DeleteMapping("/{id}")
@ApiOperation(value = "按id删除", notes = "TUserAccount删除")
@ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long", paramType = "path")
public R<Boolean> delete(@PathVariable String id){return new R<>(tUserAccountService.deleteById(id));}

/**
 * 编辑
 * @param  tUserAccount 实体
 * @return success/false
 */
@PutMapping("/update")
@ApiOperation(value = "修改 参数tProductPrice实体", notes = "TUserAccount修改")
@ApiImplicitParam(name = "tUserAccount", value = "TUserAccount实体", required = true, dataType = "TUserAccount", paramType = "path")
public R<Boolean> edit(@RequestBody TUserAccount tUserAccount){
    tUserAccount.setUpdateTime(new Date());
    return new R<>(tUserAccountService.updateById(tUserAccount));
}


}

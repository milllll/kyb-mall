package com.github.pig.goods.bussiness.product.service;

import com.github.pig.goods.bussiness.product.entity.TProductType;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 产品表 服务类
 * </p>
 *
 * @author Carlos123
 * @since 2018-11-22
 */
public interface ITProductTypeService extends IService<TProductType> {

}

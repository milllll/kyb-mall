package com.github.pig.goods.bussiness.product.service;

import com.github.pig.goods.bussiness.product.entity.TSkyLight;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 天窗（轮播图） 服务类
 * </p>
 *
 * @author tao123
 * @since 2018-09-25
 */
public interface ITSkyLightService extends IService<TSkyLight> {

}

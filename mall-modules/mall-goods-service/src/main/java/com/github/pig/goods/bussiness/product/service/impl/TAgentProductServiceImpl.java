package com.github.pig.goods.bussiness.product.service.impl;

import com.github.pig.goods.bussiness.product.entity.TAgentProduct;
import com.github.pig.goods.bussiness.product.mapper.TAgentProductMapper;
import com.github.pig.goods.bussiness.product.service.ITAgentProductService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 代理商产品关联表 服务实现类
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
@Service
public class TAgentProductServiceImpl extends ServiceImpl<TAgentProductMapper, TAgentProduct> implements ITAgentProductService {

}

package com.github.pig.goods.bussiness.product.entity;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.*;
import lombok.experimental.Accessors;


/**
 * <p>
 * 产品价格表 false
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
@Data
@EqualsAndHashCode()
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Builder
@TableName("t_product_price")
public class TProductPrice extends Model<TProductPrice>{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */

    @TableId(value = "id", type = IdType.UUID)
    @TableField("id")
    private String id;
    /**
     * 产品ID
     */

    @TableField("product_id")
    private String productId;
    /**
     * 区间
     */

    @TableField("interval")
    private String interval;
    /**
     * 单位 0：米 1：kg
     */

    @TableField("unit")
    private String unit;
    /**
     * 价格
     */

    @TableField("price")
    private Integer price;
    /**
     * 划线价
     */

    @TableField("original_price")
    private Integer originalPrice;
    /**
     * 创建时间
     */

    @TableField("create_time")
    private Date createTime;
    /**
     * 更新时间
     */

    @TableField("update_time")
    private Date updateTime;
    /**
     * 创建者
     */

    @TableField("creator")
    private String creator;
    /**
     * 更新者
     */

    @TableField("editer")
    private String editer;




    @Override
    protected Serializable pkVal() {
        return this.id;
    }



}

package com.github.pig.goods.bussiness.product.mapper;

import com.github.pig.goods.bussiness.product.entity.TAgent;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 代理商表 Mapper 接口
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
public interface TAgentMapper extends BaseMapper<TAgent> {

}

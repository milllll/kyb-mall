package com.github.pig.goods.bussiness.product.service.impl;

import com.github.pig.goods.bussiness.product.entity.TProductPrice;
import com.github.pig.goods.bussiness.product.mapper.TProductPriceMapper;
import com.github.pig.goods.bussiness.product.service.ITProductPriceService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 产品价格表 服务实现类
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
@Service
public class TProductPriceServiceImpl extends ServiceImpl<TProductPriceMapper, TProductPrice> implements ITProductPriceService {

}

package com.github.pig.goods.bussiness.product.entity;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 产品表 false
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-23
 */
@Data
@EqualsAndHashCode()
@NoArgsConstructor()
@AllArgsConstructor()
@Accessors(chain = true)
@Builder
@TableName("t_product")
public class TProduct extends Model<TProduct> {

    private static final long serialVersionUID = 1L;

    /**
     * 产品编号
     */

    @TableId(value = "id", type = IdType.UUID)
    @TableField("id")
    private String id;
    /**
     * 产品货号
     */

    @TableField("same_style_num")
    private String sameStyleNum;
    /**
     * 库存量
     */

    @TableField("product_inventory")
    private Integer productInventory;
    /**
     * 工厂代码
     */

    @TableField("factory_num")
    private String factoryNum;
    /**
     * 产品名称
     */

    @TableField("name")
    private String name;
    /**
     * 主图
     */

    @TableField("main_picture")
    private String mainPicture;
    /**
     * 详情图，最多不超过五张
     */

    @TableField("detail_picture")
    private String detailPicture;
    /**
     * 商品详情
     */

    @TableField("detail")
    private String detail;
    /**
     * 商品颜色
     */

    @TableField("colour")
    private String colour;
    /**
     * 产品大类
     */

    @TableField("product_category")
    private String productCategory;
    /**
     * 产品子类
     */

    @TableField("product_subcategory")
    private String productSubcategory;
    /**
     * 产品状态：0-待上架，1-上架，2-下架
     */




    @TableField("status")
    private String status;
    /**
     * 创建时间
     */

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @TableField("create_time")
    private Date createTime;
    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @TableField("update_time")
    private Date updateTime;
    /**
     * 创建者
     */

    @TableField("creator")
    private String creator;
    /**
     * 编辑者
     */

    @TableField("editer")
    private String editer;


    /**
     * 产品货号
     */

    @TableField("sn")
    private String sn;

    /**
     * 成分
     */

    @TableField("ingredient")
    private String ingredient;

    /**
     * 克重
     */

    @TableField("weight")
    private String weight;

    /**
     * 门幅
     */

    @TableField("size")
    private String size;

    /**
     * 用途
     */

    @TableField("use")
    private String use;

    /**
     * 工艺
     */

    @TableField("craft")
    private String craft;

    /**
     * 供应状态
     */

    @TableField("supply_status")
    private String supplyStatus;

    /**
     * 发货地点
     */

    @TableField("point_of_departure")
    private String pointOfDeparture;

    /**
     * 是否定价，默认为0
     */

    @TableField("is_has_price")
    private Integer isHasPrice = 0;




    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}

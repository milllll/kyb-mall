package com.github.pig.goods.bussiness.product.entity;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 * 代理商表 false
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
@Data
@EqualsAndHashCode()
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("t_agent")
public class TAgent extends Model<TAgent> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */

    @TableId(value = "id", type = IdType.UUID)
    @TableField("id")
    private String id;
    /**
     * 用户ID
     */

    @TableField("account_id")
    private String accountId;
    /**
     * 代理商类型,0-门店代理商，1-个人代理商
     */

    @TableField("type")
    private String type;
    /**
     * 分成提点
     */

    @TableField("di_per")
    private Double diPer;
    /**
     * 代理商名称
     */

    @TableField("name")
    private String name;
    /**
     * 代理商门店图片地址
     */

    @TableField("url")
    private String url;
    /**
     * 门店详情
     */

    @TableField("detail")
    private String detail;
    /**
     * 创建时间
     */

    @TableField("create_time")
    private Date createTime;
    /**
     * 创建者
     */

    @TableField("creator")
    private String creator;
    /**
     * 修改者
     */

    @TableField("editer")
    private String editer;
    /**
     * 修改时间
     */

    @TableField("update_time")
    private Date updateTime;


    @TableField("del_flag")
    private Integer delFlag = 0;


    /**
     * 电话
     */

    @TableField("phone")
    private String phone;

    /**
     * 地址
     */

    @TableField("address")
    private String address;

    /**
     * 详细图片
     */

    @TableField("detail_url")
    private String detailUrl;


    /**
     * 问题描述
     */

    @TableField("describe")
    private String describe;


    /**
     * 代理商状态
     */

    @TableField("status")
    private String status;



    /**
     * 代理商编号
     */

    @TableField("sn")
    private String sn;



    /**
     * 地图图片
     */

    @TableField("map_url")
    private String mapUrl;


    /**
     * 联系人
     */

    @TableField("linkman")
    private String linkman;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}

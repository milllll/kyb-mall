package com.github.pig.goods.bussiness.product.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * <p>
 *  购物车视图
 * </p>
 * @Author: Carlos
 * @param:
 * @return:
 * @Date:2019/3/14 - 2:30 PM
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VoShopCar extends TShopCar {

    private TUserAccount account;

    private TAgent agent;

    private ToProduct products;


}

package com.github.pig.goods.bussiness.product.service;

import com.github.pig.goods.bussiness.product.entity.TAgentProduct;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 代理商产品关联表 服务类
 * </p>
 *
 * @author Carlos123
 * @since 2018-08-27
 */
public interface ITAgentProductService extends IService<TAgentProduct> {

}

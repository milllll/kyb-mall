package com.github.pig.goods.bussiness.product.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.github.pig.common.util.Query;
import com.github.pig.goods.bussiness.product.entity.TOrder;
import com.github.pig.goods.bussiness.product.entity.VoOrder;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单主表 服务类
 * </p>
 *
 * @author tao123
 * @since 2018-09-13
 */
public interface ITOrderService extends IService<TOrder> {

     /**
     * <p>
     *
     * <p>
     * @Author: Carlos
     * @param:[query]
     * @return:com.baomidou.mybatisplus.plugins.Page
     */
     Page selectWithOrderPage(Query query);


     /**
      * <p>
      * selectOrderForMobile
      * </p>
      * @Author: Carlos
      * @param: query  query
      * @param: map select map
      * @return:java.util.List
      * @Date:2018/11/2 - 10:04 PM
      */
     Page selectOrderForMobile(Query query);


     /**
      * 通过ID查询产品信息
      *
      * @param id 用户ID
      * @return ToProduct
      */
     VoOrder selectOrderById( String id);

}

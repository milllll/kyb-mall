#!/usr/bin/env bash
sudo docker rm -f  cloth-mall
sudo sleep 1
sudo docker rmi   cloth-mall:v1
sudo sleep 1
cd /var/jenkins_home/workspace/cloth-mall
cp Dockerfile /dockerfile/cloth-mall
cd /dockerfile/cloth-mall
sudo docker build -t cloth-mall:v1 .
sudo sleep 1
sudo docker run -d  --restart=unless-stopped -p 9001:9001 --name cloth-mall cloth-mall:v1

